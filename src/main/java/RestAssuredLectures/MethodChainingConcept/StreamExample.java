package RestAssuredLectures.MethodChainingConcept;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamExample {
    public static void main(String[] args) {
/*
        List<String> names= Arrays.asList("Apple","Oranges","Bananas","Grapes");

        // output
        // AppleFruit, OrangesFruit, BananaFruit,GrapesFruit
        List<String> customName=new ArrayList<String>();
        for (String s: names)
        {
          customName.add(s+"Fruit");
        }
        System.out.println("Befor Names"+names);
        System.out.println("After Names"+customName);
 This code is taking more lines
  */
        //Using Stream
        List<String> names= Arrays.asList("Apple","Oranges","Bananas","Grapes");
        List<String> customNames=names.stream().map(e->e+"Fruit").collect(Collectors.toList());
        // when one method is called after another it is called method chaining
        System.out.println(customNames);

    }
}
