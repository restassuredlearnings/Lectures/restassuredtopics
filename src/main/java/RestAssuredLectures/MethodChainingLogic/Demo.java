package RestAssuredLectures.MethodChainingLogic;
class MethodRepo{
    /*
    Using this we are creating objects of method repo which will create new object unnecessary better to use reference retun this
    public MethodRepo printName(String name)
    {
        System.out.println("Name is"+name);
        return new MethodRepo()\
    }
    public MethodRepo printAge(int age)
    {
        System.out.println("Age is"+age);
        return new MethodRepo();
    }
  */
    public MethodRepo printName(String name)
    {
        System.out.println("Name is"+name);
        return this;
        // this return the reference
    }
    public MethodRepo printAge(int age)
    {
        System.out.println("Age is"+age);
        return this;
    }


}
public class Demo {
    public static void main(String[] args) {
        MethodRepo methodRepo=new MethodRepo();
        methodRepo.printAge(20).printAge(20);


    }
}
