package RestAssuredLectures.MethodChainingLogic;

public class HomePage {
    public HomePage sendFriendRequest()
    {
        System.out.println("send friend request");
        return this;
    }
    public HomePage sendMessage()
    {
        System.out.println("Send message");
        return this;
    }
}
