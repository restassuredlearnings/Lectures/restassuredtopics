package RestAssuredLectures.StaticImportExample;
/*
Static import is widely used in Rest Assured and it will be good to learn about it first. Learn in this video:-
1. What is a static import?
2. Syntax of static import.
3. Difference between import and static import?
4. When to use static import?
5. Disadvantages of static import.

Theory

What is Static Import?
O A feature that allows public and static members i.e.
fields and methods of a class to be used in Java code
without specifying the class in which the field has been
defined.
o This feature was introduced into the language in version
5.

Syntax
To reference individual public static members of a class:-
import static java.lang.Math.PI;
import static java.lang.Math.pow;
import static java.lang.System.out;
To reference all public static members of a class:-
import static java.lang.Math.*;

Difference between import and static import
The normal import declaration imports classes from
packages, allowing them to be used without package
qualification.
The static import declaration imports static members from
classes, allowing them to be used without class
qualification.
Reference -
https://docs.oracle.com/javase/7/docs/technotes/guides/la
nguage/static-import.html

When to use Static Import?
o Avoid it as much as possible.
O Good to use when you require static members from one or two classes. Like Keywords etc.
• Overuse of static import reduces readability and create confusion. Although static import concept was
introduced for better readability of code or to remove boilerplate codes.
 */
public class StaticImportJava {


}
