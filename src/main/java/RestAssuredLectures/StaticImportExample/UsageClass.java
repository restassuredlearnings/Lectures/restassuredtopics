package RestAssuredLectures.StaticImportExample;
import static RestAssuredLectures.StaticImportExample.UtilityMethods.*;

public class UsageClass {
    public static void main(String[] args) {
/*
        UtilityMethods utilityMethods=new UtilityMethods();
        utilityMethods.add(10,20);
        utilityMethods.printSomething();
Uses unenecessary space because when object is created non static members get loaded for particular object
in memory , whereas if we do the method static it will be availale from starting while doing class loading only

*/
//        UtilityMethods.add(10,20);
//        UtilityMethods.printSomething();
   /*
   So if have 100 static methods in Utility everytime i have to add
    UtilityMethods.
    which is boiler code repeating
    so we can do
    import static StaticImportExample.UtilityMethods

    so this enable us to use public and static methods in the class without the class name4

    By using staic import we can import both static variable and methods
    */
add(10,20);
printSomething();
        System.out.println(salary);
    }
}
