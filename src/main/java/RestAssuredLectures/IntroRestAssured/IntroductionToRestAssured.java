package RestAssuredLectures.IntroRestAssured;
/*
Know About Rest Assured
● Rest Assured is a Java library for testing and validating
REST services or API or Web Services.
J
● Earlier API testing was not so easy compare to Ruby and
Groovy. Rest Assured brings you that simplicity and
flexibility in Java.
O Open source
● It is built on top of HTTP Builder and supports POST,
GET, PUT, DELETE, OPTIONS, PATCH and HEAD requests.
O REST Assured is developed and maintained by Johan
Haleby. He started the project when he was working
at Jayway back in December of 2010. The project is now
sponsored by Parkster.

Versions
O Version 4.3.1 at the time of recording this video
o Since Version 3, its group id updated to io.rest-
assured from com.jayway.restassured.
o If you want to use older version (prior version 3) just
update group id.
o Try to use latest version.

Little more about Rest Assured
o Supports JSON Schema validation
o Supports JsonPath and XmlPath
o Can be integrated with Spring Mock Mvc, Spring Web Test
Client, Scala and Kotlin.
O Github - https://github.com/rest-assured/rest-assured
o Javadoc -https://www.javadoc.io/doc/io.rest-assured/rest-
assured/latest/index.html
O Release notes - https://github.com/rest-assured/rest-
assured/wiki/Release Notes
o Change log - https://raw.githubusercontent.com/rest-
assured/rest-assured/master/changelog.txt
O Buy Johan a coffee -
https://www.buymeacoffee.com/johanhaleby
 */
public class IntroductionToRestAssured {


}
