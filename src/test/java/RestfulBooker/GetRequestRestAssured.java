package RestfulBooker;

import io.restassured.RestAssured;

/*
For retrieve we use get method
 */
public class GetRequestRestAssured {

    public static void main(String[] args) {
  //      RestAssured.given().baseUri("https://restful-booker.herokuapp.com/").basePath("booking/1").when().get().then().log().all().statusCode(200);
   RestAssured.given().log().all().baseUri("https://restful-booker.herokuapp.com/").basePath("booking/{id}").pathParam("id",10).when().get().then().log().all().statusCode(200);
   //Rest assured follows the builder pattern which helps in finishing the work in a line
    }
}
