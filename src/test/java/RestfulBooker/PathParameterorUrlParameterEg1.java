package RestfulBooker;

import io.restassured.RestAssured;

/*
Path Parameters helps in genralizing instead of
https://google.com/news/india/karnataka
https://google.com/news/{countryName}/{stateName}


 */
public class PathParameterorUrlParameterEg1 {

    public static void main(String[] args) {

        RestAssured.given().log().all().
                when().get("https://restful-booker.herokuapp.com/{basePath}/{bookingId}", "booking", 2).then().log().all().statusCode(200);
        /*
        Here on the basis of index it will set automatically
        like booking will fo for basepath and 2 will go for id
        if we reverse booking and 2
        2 will go for basepath and bookingid will be booking
         */

    }
}