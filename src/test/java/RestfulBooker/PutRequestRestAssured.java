package RestfulBooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

/*
PUT Request
• A HTTP request method to update or create a resource
O It replaces the current representation of the a resource with the new request payload if resource is found
O If resource is not found on server, PUT should be capable to create a new resource
o Idempotent
• Not a safe method
o 201 - if new resource crated,
 200 or - succesfull
  204 status code - no content

primary use of put is to update the information

so in put request the payload which we sent while creating the resource same all
fields need to be sent if we we need to update one field also
so it will completely replace the new payload with the old payload

﻿

As per rfc2616:-
O The PUT method requests that the enclosed entity be stored under
the supplied Request-URI.
o If the Request-URI refers to an already existing resource,the
enclosed entity SHOULD be considered as a modified version of
the one residing on the origin server.
o If the Request-URI does not point to an existing resource,
and that URI is capable of being defined as a new resource by the
requesting user agent, the origin server can create the resource with that URI.
• If a new resource is created, the origin server MUST inform the user agent via the
201 (Created) response. If an existing resource is modified, either the 200 (OK)
or 204 (No Content) response codes SHOULD be sent to indicate successful completion of the request!
o If the resource could not be created or modified with the
Request- URI, an appropriate error response SHOULD be given that
reflects the nature of the problem.

 status code - 403 forbidden
 */
public class PutRequestRestAssured {

    public static void main(String[] args) {

        RestAssured.given().baseUri("https://restful-booker.herokuapp.com/")
                .basePath("booking/{id}").pathParam("id",1).body("{\n" +
                        "    \"firstname\" : \"Shreyansh\",\n" +
                        "    \"lastname\" : \"Singh\",\n" +
                        "    \"totalprice\" : 122,\n" +
                        "    \"depositpaid\" : false,\n" +
                        "    \"bookingdates\" : {\n" +
                        "        \"checkin\" : \"2024-01-01\",\n" +
                        "        \"checkout\" : \"2024-01-01\"\n" +
                        "    },\n" +
                        "    \"additionalneeds\" : \"Breakfast\"\n" +
                        "}").contentType(ContentType.JSON)
                .header("Authorization","Basic YWRtaW46cGFzc3dvcmQxMjM=").when()
                .put().then().log().all().statusCode(200);
    }

}
