package RestfulBooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

/*
When we have resource infromation and we want to update some part of resource or full resource
it represent what part of resource needs to be modified
 */
public class PatchRequestRestAssured {
    public static void main(String[] args) {

        RestAssured.given().baseUri("https://restful-booker.herokuapp.com/").
                pathParam("id",1)
                .basePath("booking/{id}")
                .contentType(ContentType.JSON)
                .header("Authorization","Basic YWRtaW46cGFzc3dvcmQxMjM=")
                .body("{\n" +
                        "    \"firstname\" : \"Shereyansh\",\n" +
                        "    \"lastname\" : \"Singh\"\n" +
                        "}").when()
                .patch().then().log().all().statusCode(200);


    }
}
