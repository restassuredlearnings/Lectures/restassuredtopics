package RestfulBooker;

import io.restassured.RestAssured;
/*
Only path can be replaced
for url we cannot as it is not path only with / is considered as path
 */
public class PathParameterorUrlParameterEg2 {
    public static void main(String[] args) {
        RestAssured.given().log().all().pathParam("basePath","booking").when()
                .get("https://restful-booker.herokuapp.com/{basePath}/{bookingId}",3).then().log().all().statusCode(200);
    }
}
