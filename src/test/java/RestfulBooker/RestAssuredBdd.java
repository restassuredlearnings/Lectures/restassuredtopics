package RestfulBooker;
/*
Rest Assured Script in bdd format
BDD- Behavior driven developemnt -- it is a software development process
have three main keywords
given, when , then

Given - I have a valid fb account  denotes precondtion
When - I enter wrong password       denotes action
Then - Your password is incoorect    result

 In BDD basically we write the requiremnts in such a form anyone can uderstand
 Because understanding requirement is only very difficult

 So in productionn bugs comes if requirements are not clear

 Bdd is a software development process where we write the behavior
 where we write the requirements in simple way
 */
public class RestAssuredBdd {
/*
Build Request - Given
Hit the request and get the response - When(some action)
Vlaidate the response - Then(check results)

so it is siilar to bdd manner

//given - Build Request
 RestAssured.given().baseUri("https://restful-booker.herokuapp.com/").basePath("booking").contentType(ContentType.JSON).body("{\n" +
                "    \"firstname\" : \"Shreyansh\",\n" +
                "    \"lastname\" : \"Singh\",\n" +
                "    \"totalprice\" : 100,\n" +
                "    \"depositpaid\" : false,\n" +
                "    \"bookingdates\" : {\n" +
                "        \"checkin\" : \"2023-12-12\",\n" +
                "        \"checkout\" : \"2023-12-13\"\n" +
                "    },\n" +
                "    \"additionalneeds\" : \"Breakfast\"\n" +
                "}")


// When - Hit the post request
              .when().post()

//Then - doing response validation
.then().log().all().statusCode(200);
 */
}
