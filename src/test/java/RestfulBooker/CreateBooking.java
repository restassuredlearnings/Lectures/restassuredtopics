package RestfulBooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class CreateBooking {
    public static void main(String[] args) {
        //1. Build Request
       RequestSpecification requestSpecification= RestAssured.given();
       // this request specification is empty of now , so lets start building the request, so in empty request now lets add
        // base path ,base uri and set the body

// Lets set the base uri ,  so in RequestSpecification interface we have method to set the base url and base path
        requestSpecification.baseUri("https://restful-booker.herokuapp.com/");
        requestSpecification.basePath("booking");
        requestSpecification.body("{\n" +
                "    \"firstname\" : \"Shreyansh\",\n" +
                "    \"lastname\" : \"Singh\",\n" +
                "    \"totalprice\" : 100,\n" +
                "    \"depositpaid\" : false,\n" +
                "    \"bookingdates\" : {\n" +
                "        \"checkin\" : \"2023-12-12\",\n" +
                "        \"checkout\" : \"2023-12-13\"\n" +
                "    },\n" +
                "    \"additionalneeds\" : \"Breakfast\"\n" +
                "}");
       // requestSpecification.contentType("application/json");
       // instead of this use enum type as we can do spelling mistake

       requestSpecification.contentType(ContentType.JSON);


     // also in postman once we give json body header is set automatically to json .content type
        //in which format we are sending the request but in rest assured we have to tell explicitly

        // 2. Hit Request and get Response
        //In postman we did send and request hitted with details it happened how we do it here
       Response response= requestSpecification.post();


       // we got the response but we cannot directly validate this convert it
        // into validate response
       // 3. Validate Response
         ValidatableResponse validatableResponse= response.then();
         validatableResponse.log().all();
        validatableResponse.statusCode(200);
    }
}
