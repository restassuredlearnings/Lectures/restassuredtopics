package RestfulBooker;
/*
Put vs Patch
﻿
PATCH Request
o Word explains itself
• A HTTP request method to update a resource partially or completely
• It represents what parts of resource needs to be modified
• Can be or can not be an idempotent
O Not a safe method

PUT
Replace/overwrite the current resource status completely
Requires complete payload
Can create resource if not found
Idempotent 200,201,204

PATCH
Modify existing status of a resource
Require patch payload
Can not create if not found
Can be or can not be 200,204

so see for put we have to update a particular field so in complex json it can be
tricky to update a field so better to use put as it completely dump the previous
resource and updates with the new resource

But in real time every api are there to create or do specific task
so if patch just have fields and we are sending payload with one field it is idempotent
but if it has a counter then it is a issue it will not be idempotent as count keeps
changing so it depends on how it is devloped
also if we have payload
like
{
"fname":"shreyansh"
"lastname":"singh"
}
and i give in patch
{
"lastname":""
}
then lastname will be removed only
and again if we hit it will give error

 */

public class PutVsPatch {
}
