package RestfulBooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
/*
There is a delete call which will identify the resource on server and will delete
it from the server.
﻿

DELETE Request
• Word explains itself
OA HTTP request method to delete a resource
• It may have a request body and a response body O Not a safe method
• Idempotent method
• 200(Ok), 204 (No Content), 202(Accepted) etc
त्रो
so in some delete api we may need the information for eg while deleting some
account we need to specify why we are deleting so we need body in that case
so it depends on the api how it is developed

For delete also
Any http method which manipulates the data is not safe

also delete can be hard and soft maybe they will delete permanently

Status code - 405 Method not allowed
so this is when http request to be used should be get but we are using put / post
or some another

Status code - 404
so for eg we want to delete a resource but that resource id is not present on server
then we get 404



 */
public class DeleteRequestRestAsssured {
    public static void main(String[] args) {
        RestAssured.given().baseUri("https://restful-booker.herokuapp.com/")
                .pathParam("id",2)
                .basePath("booking/{id}").log().all().
                contentType(ContentType.JSON)
                .header("Authorization","Basic YWRtaW46cGFzc3dvcmQxMjM=")
                .when().delete().then().log().all().statusCode(201);
    }
}
