package RestfulBooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class CreateBookingShorter {
    public static void main(String[] args) {
        RestAssured.given().baseUri("https://restful-booker.herokuapp.com/").basePath("booking").contentType(ContentType.JSON).body("{\n" +
                "    \"firstname\" : \"Shreyansh\",\n" +
                "    \"lastname\" : \"Singh\",\n" +
                "    \"totalprice\" : 100,\n" +
                "    \"depositpaid\" : false,\n" +
                "    \"bookingdates\" : {\n" +
                "        \"checkin\" : \"2023-12-12\",\n" +
                "        \"checkout\" : \"2023-12-13\"\n" +
                "    },\n" +
                "    \"additionalneeds\" : \"Breakfast\"\n" +
                "}").post().then().log().all().statusCode(200);
    }
}
