package RestfulBooker;

import io.restassured.RestAssured;

/*
Path Parameters helps in genralizing instead of
https://google.com/news/india/karnataka
https://google.com/news/{countryName}/{stateName}


 */
public class PathParameterorUrlParameter {

    public static void main(String[] args) {

        RestAssured.given().log().all().baseUri("https://restful-booker.herokuapp.com/").basePath("{basepath}/{bookingId}").pathParam("basepath","booking")
                .pathParam("bookingId","2").when().get().then().log().all().statusCode(200);
    }

}
